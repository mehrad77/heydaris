import React from "react";
import {Link} from "gatsby";

import Github from "../assets/github.svg";
import Telegram from "../assets/telegram.svg";
import Twitter from "../assets/twitter.svg";

import styles from './layout.module.css';

export default ({children, dir="ltr"}) =>{
  let title = dir=="ltr"? "Sajjad" : "سجاد";
  let home = dir=="ltr"?"/en":"/fa";
  let other = dir=="ltr"?"/fa":"en";
  let other_title = dir=="ltr"?"فارسی":"English";
  let foot_note = dir=="ltr"?`Hi! This is Sajjad's blog, a CS student, a Geek! Welcome to my blog!`:`سلام! من سجاد هستم. دانشجوی علوم کامپیوتر،‌ یک گیک. به وبلاگ شخصی من خوش آمدید!`;
  let foot_title = dir=="ltr"?`Sajjad 'MCSH' Heydari`:`سجاد`;
  return (
    <div dir={dir}>
      <header className={styles.header}>
        <div className={styles.wrapper}>
          <Link to={home} className={styles.site_title}>{title}</Link>
          <nav className={styles.site_nav}>
            <Link to={other} className={styles.other_title}>{other_title}</Link>
          </nav>
        </div>
      </header>
      <main className={styles.layout} dir={dir}>
        {children}
      </main>
      <footer className={styles.footer}>
        <div className={styles.wrapper}>
          <h2>{title}</h2>
          <div className={styles.footer_col_wrapper}>
            <div className={styles.footer_col1}>
              <ul className={styles.foot_list}>
                <li>{foot_title}</li>
                <li><a className={styles.foot_email} href="mailto:MCSHemail@gmail.com">MCSHemail@gmail.com</a></li>
              </ul>
            </div>
            <div className={styles.footer_col2}>
              <ul className={styles.foot_list}>
                <li className={styles.li}>
                  <a href="https://github.com/MCSH" className={styles.a}>
                    <Github className={styles.svg_icon}/>
                    <span className={styles.foot_username}>MCSH</span>
                  </a>
                </li>
                <li className={styles.li}>
                  <a href="https://twitter.com/sajjad_heydari" className={styles.a}>
                    <Twitter className={styles.svg_icon}/>
                    <span className={styles.foot_username}>Sajjad_Heydari</span>
                  </a>
                </li>
                <li className={styles.li}>
                  <a href="https://t.me/pi_developer" className={styles.a}>
                    <Telegram className={styles.svg_icon}/>
                    <span className={styles.foot_username}>pi_developer</span>
                  </a>
                </li>
              </ul>
            </div>
            <div className={styles.footer_col3}>
              <p>
                {foot_note}
              </p>
            </div>
          </div>
        </div>
      </footer>
    </div>
  );
}
