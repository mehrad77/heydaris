import React from "react";
export default (props) => (
  <ul style={{marginLeft: 0, listStyle: 'None', padding: 0}}>
    {props.children}
  </ul>
);
