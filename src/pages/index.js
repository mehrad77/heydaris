import React, {useEffect} from "react";
import Layout from "../components/layout";
import {Link, navigate} from "gatsby";

let tmp = () => (
  <Layout>
    <h2>Hello World!</h2>
    <Link to="/blog">Blog</Link>
  </Layout>);

export default () => {
  useEffect(() => {
    navigate('/en/');
  }, []);
  return null;
};
