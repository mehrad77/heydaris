const path = require(`path`);

exports.createPages = async ({ actions, graphql }) => {
  const { createPage } = actions;

  let result = await graphql(`
    {
      allMarkdownRemark(filter: {fileAbsolutePath: {regex: "/blog_en/"}}) {
        edges {
          node {
            frontmatter {
              path
            }
          }
        }
      }
    }
  `);
  if (result.errors) {
    console.error(result.errors);
  }

  result.data.allMarkdownRemark.edges.forEach(({ node }) => {
    createPage({
      path: "/en/" + node.frontmatter.path,
      component: path.resolve(`src/templates/en_post.js`),
      context:{
        file: node.frontmatter.path,
      },
    });
  });

  result = await graphql(`
    {
      allMarkdownRemark(filter: {fileAbsolutePath: {regex: "/blog_fa/"}}) {
        edges {
          node {
            frontmatter {
              path
            }
          }
        }
      }
    }
  `);
  if (result.errors) {
    console.error(result.errors);
  }

  result.data.allMarkdownRemark.edges.forEach(({ node }) => {
    createPage({
      path: "/fa/" + node.frontmatter.path,
      component: path.resolve(`src/templates/fa_post.js`),
      context:{
        file: node.frontmatter.path,
      },
    });
  });
};
