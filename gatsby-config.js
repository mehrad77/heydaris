module.exports = {
  plugins: [
    'gatsby-transformer-remark',
    {
      resolve: 'gatsby-source-filesystem',
      options: {
        name: 'blog_en',
        path: `${__dirname}/content/blog_en`
      },
    },
    {
      resolve: 'gatsby-source-filesystem',
      options: {
        name: 'blog_fa',
        path: `${__dirname}/content/blog_fa`
      },
    },
    {
      resolve: 'gatsby-plugin-react-svg',
      options: {
        rule: {
          include: /assets/,
        },
      },
    },
    {
      resolve: 'gatsby-plugin-google-analytics',
      options:{
        trackingId: "UA-102624858-4",
      },
    }
  ],
};
